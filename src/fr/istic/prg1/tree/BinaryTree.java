package fr.istic.prg1.tree;

import fr.istic.prg1.tree_util.Iterator;
import fr.istic.prg1.tree_util.NodeType;

import java.util.Stack;
import java.util.concurrent.ArrayBlockingQueue;

public class BinaryTree<T> {

    protected final BinaryTree<T>.Root element;
    private static boolean firstTime = true;

    public BinaryTree() {
        if (firstTime) firstTime = false;
        this.element = new Root();
    }

    public Iterator<T> iterator() {
        return new Element();
    }

    public boolean isEmpty() {
        return this.element.isEmpty();
    }

    public String toString() {
        StringBuilder res = new StringBuilder();
        ArrayBlockingQueue<Root> q = new ArrayBlockingQueue<>(1000);
        Root finLigne = new Root();

        try {
            q.put(this.element);
            q.put(finLigne);

            while(!q.isEmpty()) {
                Root a = q.take();
                if (a == finLigne) {
                    res.append("\n");
                    if (!q.isEmpty()) {
                        q.put(finLigne);
                    }
                } else if (a.isEmpty()) {
                    res.append("* ");
                } else {
                    res.append(a.nextRoot.toString()).append(" ");
                    q.put(a.doubleRoot);
                    q.put(a.simpleRoot);
                }
            }
        } catch (InterruptedException err) {
            System.out.println(err);
            System.exit(0);
        }

        return res.toString();
    }


    public class Element implements Iterator<T> {
        private Root root;
        private final Stack<Root> stack;

        private Element() {
            this.stack = new Stack<>();
            this.root = BinaryTree.this.element;
        }

        public void goLeft() {
            try {
                assert !this.isEmpty() : "le butoir n'a pas de fils";
                assert this.root.doubleRoot != null : "le fils gauche d'existe pas";
            } catch (AssertionError err) {
                err.printStackTrace();
                System.exit(0);
            }
            this.stack.push(this.root);
            this.root = this.root.doubleRoot;
        }

        public void goRight() {
            try {
                assert !this.isEmpty() : "le butoir n'a pas de fils";
                assert this.root.simpleRoot != null : "le fils droit d'existe pas";
            } catch (AssertionError err) {
                err.printStackTrace();
                System.exit(0);
            }
            this.stack.push(this.root);
            this.root = this.root.simpleRoot;
        }

        public void goUp() {
            try {
                assert !this.stack.empty() : " la racine n'a pas de pere";
            } catch (AssertionError var2) {
                var2.printStackTrace();
                System.exit(0);
            }
            this.root = this.stack.pop();
        }

        public void goRoot() {
            this.stack.clear();
            this.root = BinaryTree.this.element;
        }

        public boolean isEmpty() {
            return this.root.isEmpty();
        }

        public NodeType nodeType() {
            if (this.isEmpty()) return NodeType.SENTINEL;
            else if (this.root.doubleRoot.isEmpty()) return this.root.simpleRoot.isEmpty() ? NodeType.LEAF : NodeType.SIMPLE_RIGHT;
            else return this.root.simpleRoot.isEmpty() ? NodeType.SIMPLE_LEFT : NodeType.DOUBLE;
        }

        public void remove() {
            try {
                assert this.nodeType() != NodeType.DOUBLE : "retirer : retrait d'un noeud double non permis";
            } catch (AssertionError err) {
                err.printStackTrace();
                System.exit(0);
            }

            Root sentinel = null;
            switch(this.nodeType().ordinal()) {
                case 1:
                    sentinel = new Root();
                    break;
                case 2:
                    sentinel = this.root.doubleRoot;
                    break;
                case 3:
                    sentinel = this.root.simpleRoot;
                case 4:
                default:
                    break;
                case 5:
                    return;
            }

            assert sentinel != null;
            this.root.nextRoot = sentinel.nextRoot;
            this.root.doubleRoot = sentinel.doubleRoot;
            this.root.simpleRoot = sentinel.simpleRoot;
        }

        public void clear() {
            this.root.nextRoot = null;
            this.root.doubleRoot = null;
            this.root.simpleRoot = null;
        }

        public T getValue() {
            return this.root.nextRoot;
        }

        public void addValue(T v) {
            try {
                assert this.isEmpty() : "Ajouter : on n'est pas sur un butoir";
            } catch (AssertionError err) {
                err.printStackTrace();
                System.exit(0);
            }

            this.root.nextRoot = v;
            this.root.doubleRoot = new Root();
            this.root.simpleRoot = new Root();
        }

        public void setValue(T v) {
            this.root.nextRoot = v;
        }

        private void ancestor(int i, int j) {
            try {
                assert !this.stack.empty() : "switchValue : argument trop grand";
            } catch (AssertionError err) {
                err.printStackTrace();
                System.exit(0);
            }

            Root x = this.stack.pop();
            if (j < i) {
                this.ancestor(i, j + 1);
            } else {
                T v = x.nextRoot;
                x.nextRoot = this.root.nextRoot;
                this.root.nextRoot = v;
            }
            this.stack.push(x);
        }

        public void switchValue(int i) {
            try {
                assert i >= 0 : "switchValue : argument negatif";
            } catch (AssertionError err) {
                err.printStackTrace();
                System.exit(0);
            }
            if (i > 0) this.ancestor(i, 1);
        }
    }

    private class Root {
        public T nextRoot = null;
        public BinaryTree<T>.Root doubleRoot = null;
        public BinaryTree<T>.Root simpleRoot = null;

        public Root() {

        }

        public boolean isEmpty() {
            return this.doubleRoot == null && this.simpleRoot == null;
        }
    }
}
