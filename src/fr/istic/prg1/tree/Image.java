package fr.istic.prg1.tree;

import java.util.Scanner;

import fr.istic.prg1.tree_util.AbstractImage;
import fr.istic.prg1.tree_util.Iterator;
import fr.istic.prg1.tree_util.Node;
import fr.istic.prg1.tree_util.NodeType;

public class Image extends AbstractImage {

    private static final Scanner standardInput = new Scanner(System.in);

    public Image() {
        super();
    }

    public static void closeAll() {
        standardInput.close();
    }

    /**
     * @param x abscisse du point
     * @param y ordonnée du point
     * @return true, si le point (x, y) est allumé dans this, false sinon
     * @pre !this.isEmpty()
     */
    @Override
    public boolean isPixelOn(int x, int y) {
        Iterator<Node> it = this.iterator();
        int minX = 0, minY = 0, maxX = 256, maxY = 256;

        while (!it.isEmpty()) {
            int cutY = (maxY + minY) / 2;
            int cutX = (maxX + minX) / 2;

            if (maxX - minX == maxY - minY) { // Horizontal
                if (y < cutY) {
                    it.goLeft();  // Haut
                    maxY = cutY;
                } else {
                    it.goRight();  // Bas
                    minY = cutY;
                }
            } else { // Vertical
                if (x < cutX) {
                    it.goLeft();  // Gauche
                    maxX = cutX;
                } else {
                    it.goRight();  // Droite
                    minX = cutX;
                }
            }
        }
        it.goUp();
        return it.getValue().equals(Node.valueOf(1));
    }

    /**
     * this devient identique à image2.
     *
     * @param image image à copier
     * @pre !image2.isEmpty()
     */
    @Override
    public void affect(AbstractImage image) {
        Iterator<Node> it1 = image.iterator();
        Iterator<Node> it2 = this.iterator();
        it2.clear();
        if (!it1.isEmpty()) affectAux(it1, it2);
    }

    private void affectAux(Iterator<Node> it1, Iterator<Node> it2) {
        if (!it1.isEmpty()) {
            it2.addValue(it1.getValue());

            it1.goLeft();
            it2.goLeft();
            affectAux(it1, it2);
            it1.goUp();
            it2.goUp();
            it1.goRight();
            it2.goRight();
            affectAux(it1, it2);
            it1.goUp();
            it2.goUp();
        }
    }

    /**
     * this devient rotation de image2 à 180 degrés.
     *
     * @param image2 image pour rotation
     * @pre !image2.isEmpty()
     */
    @Override
    public void rotate180(AbstractImage image2) {
        Iterator<Node> it = image2.iterator();
        Iterator<Node> it2 = this.iterator();
        it2.clear();
        if (!it.isEmpty()) rotateAux180(it, it2);
    }

    private void rotateAux180(Iterator<Node> it, Iterator<Node> it2) {
        if (!it.isEmpty()) {
            it2.addValue(it.getValue());

            it2.goRight();
            it.goLeft();
            rotateAux180(it, it2);
            it2.goUp();
            it.goUp();
            it2.goLeft();
            it.goRight();
            rotateAux180(it, it2);
            it2.goUp();
            it.goUp();
        }
    }

    /**
     * this devient rotation de image2 à 90 degrés dans le sens des aiguilles
     * d'une montre.
     *
     * @param image2 image pour rotation
     * @pre !image2.isEmpty()
     */
    @Override
    public void rotate90(AbstractImage image2) {
        System.out.println();
        System.out.println("-------------------------------------------------");
        System.out.println("Fonction non demandée");
        System.out.println("-------------------------------------------------");
        System.out.println();
    }

    /**
     * this devient inverse vidéo de this, pixel par pixel.
     *
     * @pre !image.isEmpty()
     */
    @Override
    public void videoInverse() {
        Iterator<Node> it = this.iterator();
        videoInverseAux(it);
    }

    private void videoInverseAux(Iterator<Node> it) {
        if (!it.isEmpty()) {
            if (it.getValue().state == 1) it.setValue(Node.valueOf(0));
            else if (it.getValue().state == 0) it.setValue(Node.valueOf(1));
            else {
                it.goLeft();
                videoInverseAux(it);
                it.goUp();
                it.goRight();
                videoInverseAux(it);
                it.goUp();
            }
        }
    }

    /**
     * this devient image miroir verticale de image2.
     *
     * @param image2 image à agrandir
     * @pre !image2.isEmpty()
     */
    @Override
    public void mirrorV(AbstractImage image2) {
        Iterator<Node> it1 = image2.iterator();
        Iterator<Node> it2 = this.iterator();
        it2.clear();
        if (!it1.isEmpty()) mirrorVAux(it1, it2, 1);
    }

    private void mirrorVAux(Iterator<Node> it1, Iterator<Node> it2, int i) {
        if (!it1.isEmpty()) {
            it2.addValue(it1.getValue());

            it2.goLeft();
            if (i % 2 == 1) it1.goRight();
            else it1.goLeft();
            mirrorVAux(it1, it2, i + 1);
            it2.goUp();
            it1.goUp();
            it2.goRight();
            if (i % 2 == 1) it1.goLeft();
            else it1.goRight();
            mirrorVAux(it1, it2, i + 1);
            it2.goUp();
            it1.goUp();
        }
    }

    /**
     * this devient image miroir horizontale de image2.
     *
     * @param image2 image à agrandir
     * @pre !image2.isEmpty()
     */
    @Override
    public void mirrorH(AbstractImage image2) {
        Iterator<Node> it1 = image2.iterator();
        Iterator<Node> it2 = this.iterator();
        it2.clear();
        if (!it1.isEmpty()) mirrorHAux(it1, it2, 1);
    }

    private void mirrorHAux(Iterator<Node> it1, Iterator<Node> it2, int i) {
        if (!it1.isEmpty()) {
            it2.addValue(it1.getValue());

            it2.goLeft();
            if (i % 2 == 0) it1.goRight();
            else it1.goLeft();
            mirrorHAux(it1, it2, i + 1);
            it2.goUp();
            it1.goUp();
            it2.goRight();
            if (i % 2 == 0) it1.goLeft();
            else it1.goRight();
            mirrorHAux(it1, it2, i + 1);
            it2.goUp();
            it1.goUp();
        }
    }

    /**
     * this devient quart supérieur gauche de image2.
     *
     * @param image2 image à agrandir
     * @pre !image2.isEmpty()
     */
    @Override
    public void zoomIn(AbstractImage image2) {
        Iterator<Node> it1 = image2.iterator();
        Iterator<Node> it2 = this.iterator();
        it2.clear();

        if (!it1.isEmpty()) {
            if (it1.getValue().state == 2) {
                it1.goLeft();
                if (it1.getValue().state == 2) {
                    it1.goLeft();
                    if (it1.getValue().state == 2) affectAux(it1, it2);
                    else it2.addValue(Node.valueOf(0));
                } else it2.addValue(it1.getValue());
            } else it2.addValue(it1.getValue());
        }
    }

    /**
     * Le quart supérieur gauche de this devient image2, le reste de this
     * devient éteint.
     *
     * @param image2 image à réduire
     * @pre !image2.isEmpty()
     */
    @Override
    public void zoomOut(AbstractImage image2) {
        Iterator<Node> it1 = image2.iterator();
        Iterator<Node> it2 = this.iterator();
        it2.clear();

        if (!it1.isEmpty()) {
            it2.addValue(Node.valueOf(2));
            it2.goRight();
            it2.addValue(Node.valueOf(0));
            it2.goUp();
            it2.goLeft();
            it2.addValue(Node.valueOf(2));
            it2.goRight();
            it2.addValue(Node.valueOf(0));
            it2.goUp();
            it2.goLeft();
            zoomOutAux(it1, it2, 0);

            it2.goRoot();
            compactTree(it2);
        }
    }

    private void zoomOutAux(Iterator<Node> it1, Iterator<Node> it2, int i) {
        if (!it1.isEmpty()) {
            if (i < 14) {
                if (it1.getValue().state == 2) {
                    it2.addValue(it1.getValue());

                    it1.goLeft();
                    it2.goLeft();
                    zoomOutAux(it1, it2, i + 1);
                    int left = it2.getValue().state;
                    it1.goUp();
                    it2.goUp();
                    it1.goRight();
                    it2.goRight();
                    zoomOutAux(it1, it2, i + 1);
                    int right = it2.getValue().state;
                    it1.goUp();
                    it2.goUp();
                    if (left == right && right != 2) {
                        it2.clear();
                        it2.addValue(Node.valueOf(right));
                    }
                } else it2.addValue(it1.getValue());
            } else {
                if (it1.getValue().state == 2) {
                    it1.goLeft();
                    int left = it1.getValue().state;
                    it1.goUp();
                    it1.goRight();
                    int right = it1.getValue().state;
                    it1.goUp();
                    if ((left == 0 && right == 0) || (left == 0 && right == 2) || (left == 2 && right == 0)) it2.addValue(Node.valueOf(0));
                    else it2.addValue(Node.valueOf(1));
                } else {
                    it2.addValue(it1.getValue());
                }
            }
        }
    }

    private void compactTree(Iterator<Node> it) {
        if (it.getValue().state == 2) {
            it.goLeft();
            compactTree(it);
            int left = it.getValue().state;
            it.goUp();
            it.goRight();
            compactTree(it);
            int right = it.getValue().state;
            it.goUp();
            if (right == left && right != 2) {
                it.clear();
                it.addValue(Node.valueOf(right));
            }
        }
    }

    /**
     * this devient l'intersection de image1 et image2 au sens des pixels
     * allumés.
     *
     * @param image1 premiere image
     * @param image2 seconde image
     * @pre !image1.isEmpty() && !image2.isEmpty()
     */
    @Override
    public void intersection(AbstractImage image1, AbstractImage image2) {
        Iterator<Node> it1 = image1.iterator();
        Iterator<Node> it2 = image2.iterator();
        Iterator<Node> it3 = this.iterator();
        it3.clear();

        if (!it1.isEmpty() && !it2.isEmpty()) intersectionAux(it1, it2, it3);
    }

    private void intersectionAux(Iterator<Node> it1, Iterator<Node> it2, Iterator<Node> it3) {
        if (!it1.isEmpty() && !it2.isEmpty()) {
            if (it1.getValue().state == 0 || it2.getValue().state == 0) it3.addValue(Node.valueOf(0));
            else if (it1.getValue().state == 1 && it2.getValue().state == 1) it3.addValue(it1.getValue());
            else {
                if (it1.getValue().state == 1) affectAux(it2, it3);
                else if (it2.getValue().state == 1) affectAux(it1, it3);
                else {
                    it3.addValue(Node.valueOf(2));

                    it3.goLeft();
                    it2.goLeft();
                    it1.goLeft();
                    intersectionAux(it1, it2, it3);
                    int left = it3.getValue().state;
                    it1.goUp();
                    it2.goUp();
                    it3.goUp();

                    it1.goRight();
                    it2.goRight();
                    it3.goRight();
                    intersectionAux(it1, it2, it3);
                    int right = it3.getValue().state;
                    it1.goUp();
                    it2.goUp();
                    it3.goUp();

                    if (left == right && left != 2) {
                        it3.clear();
                        it3.addValue(Node.valueOf(left));
                    }
                }
            }
        }
    }

    /**
     * this devient l'union de image1 et image2 au sens des pixels allumés.
     *
     * @param image1 premiere image
     * @param image2 seconde image
     * @pre !image1.isEmpty() && !image2.isEmpty()
     */
    @Override
    public void union(AbstractImage image1, AbstractImage image2) {
        Iterator<Node> it1 = image1.iterator();
        Iterator<Node> it2 = image2.iterator();
        Iterator<Node> it3 = this.iterator();
        it3.clear();

        if (!it1.isEmpty() && !it2.isEmpty()) {
            unionAux(it1, it2, it3);
        } else if (it1.isEmpty()) affectAux(it2, it3);
        else if (it2.isEmpty()) affectAux(it1, it3);
    }

    private void unionAux(Iterator<Node> it1, Iterator<Node> it2, Iterator<Node> it3) {
        if (!it1.isEmpty() && !it2.isEmpty()) {
            if (it1.getValue().state == 1 || it2.getValue().state == 1) it3.addValue(Node.valueOf(1));
            else if (it1.getValue().state == 0 && it2.getValue().state == 0) it3.addValue(it1.getValue());
            else {
                if (it1.getValue().state == 0) affectAux(it2, it3);
                else if (it2.getValue().state == 0) affectAux(it1, it3);
                else {
                    it3.addValue(Node.valueOf(2));

                    it3.goLeft();
                    it2.goLeft();
                    it1.goLeft();
                    unionAux(it1, it2, it3);
                    int left = it3.getValue().state;
                    it1.goUp();
                    it2.goUp();
                    it3.goUp();

                    it1.goRight();
                    it2.goRight();
                    it3.goRight();
                    unionAux(it1, it2, it3);
                    int right = it3.getValue().state;
                    it1.goUp();
                    it2.goUp();
                    it3.goUp();

                    if (left == right && left != 2) {
                        it3.clear();
                        it3.addValue(Node.valueOf(left));
                    }
                }
            }
        }
    }

    /**
     * Attention : cette fonction ne doit pas utiliser la commande isPixelOn
     *
     * @return true si tous les points de la forme (x, x) (avec 0 <= x <= 255)
     * sont allumés dans this, false sinon
     */
    @Override
    public boolean testDiagonal() {
        Iterator<Node> it = this.iterator();
        return testDiagonalAux(it, true, true);
    }

    private boolean testDiagonalAux(Iterator<Node> it, boolean decoupe, boolean gauche) {
        if (!it.isEmpty()) {
            if (it.getValue().state == 1) return true;
            if (decoupe) {
                if (it.getValue().state == 2) {
                    it.goLeft();
                    boolean firstQuarter = testDiagonalAux(it, false, true);
                    it.goUp();
                    it.goRight();
                    boolean lastQuarter = testDiagonalAux(it, false, false);
                    it.goUp();
                    return firstQuarter && lastQuarter;
                }
            } else {
                if (it.getValue().state == 2) {
                    if (gauche) it.goLeft();
                    else it.goRight();
                    boolean test = testDiagonalAux(it, true, gauche);
                    it.goUp();
                    return test;
                } else return false;
            }
        }
        return false;
    }


    /**
     * @param x1 abscisse du premier point
     * @param y1 ordonnée du premier point
     * @param x2 abscisse du deuxième point
     * @param y2 ordonnée du deuxième point
     * @return true si les deux points (x1, y1) et (x2, y2) sont représentés par
     * la même feuille de this, false sinon
     * @pre !this.isEmpty()
     */
    @Override
    public boolean sameLeaf(int x1, int y1, int x2, int y2) {
        Iterator<Node> it = this.iterator();
        int x = 128, y = 128, heightX = 128, heightY = 128;
        while (!it.nodeType().equals(NodeType.LEAF)) {
            if (heightX - x == heightY - y) {
                if (y1 < y && y2 < y) {
                    it.goLeft();
                    y -= heightY / 2;
                } else if (y1 >= y && y2 >= y) {
                    it.goRight();
                    y += heightY / 2;
                } else return false;
                heightY /= 2;
            } else {
                if (x1 < x && x2 < x) {
                    it.goLeft();
                    x -= heightX / 2;
                } else if (x1 >= x && x2 >= x) {
                    it.goRight();
                    x += heightX / 2;
                } else return false;
                heightX /= 2;
            }
        }
        return true;
    }

    /**
     * @param image2 autre image
     * @return true si this est incluse dans image2 au sens des pixels allumés
     * false sinon
     * @pre !this.isEmpty() && !image2.isEmpty()
     */
    @Override
    public boolean isIncludedIn(AbstractImage image2) {
        Iterator<Node> it1 = image2.iterator();
        Iterator<Node> it2 = this.iterator();
        if (!it1.isEmpty() && !it2.isEmpty()) return isIncludeInAux(it1, it2);
        else return false;
    }

    private boolean isIncludeInAux(Iterator<Node> it1, Iterator<Node> it2) {
        if (!it1.isEmpty() && !it2.isEmpty()) {
            if (it2.getValue().state == 1 && (it1.getValue().state == 2 || it1.getValue().state == 0)) return false;
            else if (it2.getValue().state == 2 && it1.getValue().state == 0) return false;
            else {
                it1.goLeft();
                it2.goLeft();
                if (isIncludeInAux(it1, it2)) {
                    it1.goUp();
                    it2.goUp();
                    it1.goRight();
                    it2.goRight();
                    if (!isIncludeInAux(it1, it2)) return false;
                } else return false;
                it1.goUp();
                it2.goUp();
            }
        }
        return true;
    }

}
